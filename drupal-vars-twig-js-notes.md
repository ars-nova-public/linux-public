# Sending env vars from settings.php to twig and js

${toc}

## Goals

- Set up twig debugging
- Set up variables
  - Create environment variables in settings.local.php
  - Send to twig template
  - Send to theme .js

## Steps

1. enable twig debugging
   1. copy default.services.yml services.yml
   2. edit and enable debugging
1. local settings
   1. copy example.settings.local.php to default/settings.local.php
   2. add `exit;` to test
1. edit settings.php
   1. uncommenting settings.local.php include at bottom
   2. add env variable (below)
1. add [theme_preprocess_node()](https://api.drupal.org/api/drupal/core%21modules%21node%21node.module/function/template_preprocess_node/9.3.x) to .theme file
1. edit node.twig
1. edit script.js

## Components

```
# enable debugging
$settings['env']['environment'] = 'local';
$settings['env']['url_services'] = 'http://local.example.com/';

# script.js
console.table(settings.env);

# node.twig
{% if env.environment=='local' %}
  this is local
{% endif %}

# ------------------------------------------
# theme preprocess function
# ------------------------------------------
 use Drupal\Core\Site\Settings;
 /**
 * Implements template_preprocess_node().
 */
function my_subtheme_preprocess_node(&$variables) {

  $node = $variables['node'];
  $node_type = $node->getType();

  if ($node_type === 'page') {
    // get from settings.local.php
    $env = Settings::get('env');

    // Send  to template.
    $variables['env'] = $env;

    // Send to js, via settings.env
    $variables['#attached']['drupalSettings']['env'] = $env;
  }

}

```
